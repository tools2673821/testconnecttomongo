const mongoose = require("mongoose");
const mode = process.env.NODE_ENV || "dev";
const configDB = require("./config/db.json")[mode];
const configWA = require("./config/wa.json");
const axios = require("axios");

async function testConnection(db) {
  const authMongodb = db.password ? `${db.username}:${db.password}@` : "";

  try {
    const conn = await mongoose.connect(
      `mongodb://${authMongodb}${db.host}:${db.port}/${db.db}`,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      }
    );
    console.log(`[!] MongoDB connected to ${db.host}:${db.port}`);
    conn.disconnect();
  } catch (error) {
    console.log(`[!] Error connection to ${db.host}:${db.port}: `, error);
    // kirim api notif WA
    axios
      .post(
        configWA.url,
        {
          pesan: `[!] Error connection to ${db.host}:${db.port} `,
          tujuan: configWA.destination,
        },
        { headers: { token: configWA.token } }
      )
      .then((respose) => console.log("send message error to WA"))
      .catch((err) => console.log("error :", err));
  }
}

async function testConnections() {
  for (const db of configDB) {
    await testConnection(db);
  }
}

testConnections();
